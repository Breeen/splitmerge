#ifndef THREADPOOL_HPP
#define THREADPOOL_HPP

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <functional>

template <typename T>
class ThreadPool {
public:
    ThreadPool(int nbThreads) {
        for (int i = 0; i < nbThreads; i++) {
            threads.push_back(std::thread(&ThreadPool<T>::threadLoop, this));
        }
        // usefull if you have continuous tasks and a thread waiting for results to stop all the waiting threads
        threadStop = std::thread(&ThreadPool<T>::checkStop, this);
    }

    ~ThreadPool() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            stop = true;
        }
        condition.notify_all();
        for (std::thread& thread : threads) {
            thread.join();
        }
        threadStop.join();
    }

    void addTask(std::function<T()> task) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            tasks.push(task);
        }
        condition.notify_one();
    }

    void stopTasks() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            stop = true;
        }
        condition.notify_all();
    }

    T getTaskResult() {
        std::unique_lock<std::mutex> lock(mutex);
        waitingThreads++;
        condition.wait(lock, [this] { return !results.empty() || stop; });
        waitingThreads--;
        if (stop && results.empty()) {
            return T();
        }
        T result = results.front();
        results.pop();
        return result;
    }

    int getNbResults() {
        std::lock_guard<std::mutex> lock(mutex);
        return results.size();
    }
    bool hasResults() {
        std::lock_guard<std::mutex> lock(mutex);
        return !results.empty();
    }
    bool hasTasks() {
        std::lock_guard<std::mutex> lock(mutex);
        return !tasks.empty();
    }

private:
    void threadLoop() {
        while (true) {
            std::unique_lock<std::mutex> lock(mutex);
            waitingThreads++;
            condition.wait(lock, [this] { return stop || !tasks.empty(); });
            waitingThreads--;
            if (stop && tasks.empty()) {
                return;
            }
            std::function<T()> task = tasks.front();
            tasks.pop();
            lock.unlock();
            T result = task();
            lock.lock();
            results.push(result);
            lock.unlock();
            condition.notify_one();
        }
    }

    void checkStop() {
        int count = 0;
        while (!stop) {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            std::unique_lock<std::mutex> lock(mutex);
            bool test = (waitingThreads == static_cast<int>(threads.size()) + 1);
            lock.unlock();
            if (test && !stop && !hasTasks() && !hasResults()) {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    stop = true;
                }
                condition.notify_all();
            }
            if (count % 10 == 0) {
                condition.notify_all();
            }
            count++;
        }
    }

    std::vector<std::thread> threads;
    std::thread threadStop;
    std::queue<std::function<T()>> tasks;
    std::queue<T> results;
    std::mutex mutex;
    std::condition_variable condition;
    bool stop;
    int waitingThreads;
};

template <>
class ThreadPool<void> {
public:
    ThreadPool(int nbThreads) {
        for (int i = 0; i < nbThreads; i++) {
            threads.push_back(std::thread(&ThreadPool<void>::threadLoop, this));
        }
    }

    ~ThreadPool() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            stop = true;
        }
        condition.notify_all();
        for (std::thread& thread : threads) {
            thread.join();
        }
    }

    void addTask(std::function<void()> task) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            tasks.push(task);
        }
        condition.notify_one();
    }

    void stopTasks() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            stop = true;
        }
        condition.notify_all();
    }

    void getTaskResult() {
        return;
    }

    int getNbResults() {
        return 0;
    }
    bool hasResults() {
        return false;
    }
    bool hasTasks() {
        std::lock_guard<std::mutex> lock(mutex);
        return !tasks.empty();
    }

private:
    void threadLoop() {
        while (true) {
            std::unique_lock<std::mutex> lock(mutex);
            condition.wait(lock, [this] { return stop || !tasks.empty(); });
            if (stop && tasks.empty()) {
                return;
            }
            std::function<void()> task = tasks.front();
            tasks.pop();
            lock.unlock();
            task();
            condition.notify_one();
        }
    }

    std::vector<std::thread> threads;
    std::queue<std::function<void()>> tasks;
    std::mutex mutex;
    std::condition_variable condition;
    bool stop;
};

#endif
