#include "zone.hpp"

Zone::Zone(int x, int y, int width, int height, cv::Mat& img, std::function<bool(Pixel, Pixel)> compareColor) : x(x), y(y), width(width), height(height), uniform(NOT_YET_CALCULATED), img(img), compareColor(compareColor) {}

Zone::Zone(const Zone& zone) : x(zone.x), y(zone.y), width(zone.width), height(zone.height), uniform(zone.uniform), color(zone.color), img(zone.img), compareColor(zone.compareColor) {}

Zone::~Zone() {
}

int Zone::getX() const {
    return x;
}

int Zone::getY() const {
    return y;
}

int Zone::getWidth() const {
    return width;
}

int Zone::getHeight() const {
    return height;
}

Pixel Zone::getColor() const {
    return color;
}

bool Zone::isUniform() {
    if (uniform == NOT_YET_CALCULATED) {
        uniform = UNIFORM;
        for (int i = x; i < x + width; i++) {
            for (int j = y; j < y + height; j++) {
                if (!compareColor(img.at<cv::Vec3b>(i, j), img.at<cv::Vec3b>(x, y))) {
                    uniform = NOT_UNIFORM;
                    return false;
                }
            }
        }
        unsigned char r = img.at<cv::Vec3b>(x,y)[2];
        unsigned char g = img.at<cv::Vec3b>(x,y)[1];
        unsigned char b = img.at<cv::Vec3b>(x,y)[0];
        color = Pixel(static_cast<int>(r), static_cast<int>(g), static_cast<int>(b));
    }
    return uniform == UNIFORM;
}