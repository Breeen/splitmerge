#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>
#include <vector>

#include "zone.hpp"

class Node {
public:
    Node(Node* parent, Zone* zone);
    ~Node();

    std::vector<Node*> getChildren();
    Node* getParent();
    Zone* getZone();
    bool isLeaf();

    void addChild(Node* child);
    void setParent(Node* parent);

private:
    std::vector<Node*> children;
    Node* parent;
    Zone* zone;
};

#endif