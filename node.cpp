#include "node.hpp"

Node::Node(Node* parent, Zone* zone) : parent(parent), zone(zone) {
}

Node::~Node() {
    delete zone;
    for (Node* child : children) {
        delete child;
    }
}

std::vector<Node*> Node::getChildren() {
    return children;
}

Node* Node::getParent() {
    return parent;
}

Zone* Node::getZone() {
    return zone;
}

bool Node::isLeaf() {
    return children.empty();
}

void Node::addChild(Node* child) {
    children.push_back(child);
}

void Node::setParent(Node* parent) {
    this->parent = parent;
}