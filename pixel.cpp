#include "pixel.hpp"

Pixel::Pixel() : r(0), g(0), b(0) {}

Pixel::Pixel(int r, int g, int b) : r(r), g(g), b(b) {}

Pixel::Pixel(const cv::Vec3b& color) : r(color[2]), g(color[1]), b(color[0]) {}

int Pixel::getR() const {
    return r;
}

int Pixel::getG() const {
    return g;
}

int Pixel::getB() const {
    return b;
}

void Pixel::setR(int r) {
    this->r = r;
}

void Pixel::setG(int g) {
    this->g = g;
}

void Pixel::setB(int b) {
    this->b = b;
}

bool Pixel::operator==(const Pixel& pixel) const {
    return r == pixel.r && g == pixel.g && b == pixel.b;
}