#include "splitmerge.hpp"

#include <stack>
#include <queue>

SplitMerge::SplitMerge(cv::Mat& img) : img(img), splitDone(false), mergeDone(false) {
    compareColor = [](Pixel p1, Pixel p2) {
        return p1 == p2;
    };
    root = new Node(nullptr, new Zone(0, 0, img.rows, img.cols, img, compareColor));
}

SplitMerge::~SplitMerge() {
    delete root;
}

Node* SplitMerge::getRoot() {
    return root;
}

int SplitMerge::countLeaves() {
    int count = 0;
    std::stack<Node*> nodes;
    nodes.push(root);
    while (!nodes.empty()) {
        Node* node = nodes.top();
        nodes.pop();
        if (node->isLeaf()) {
            count++;
        } else {
            nodes.push(node->getChildren()[0]);
            nodes.push(node->getChildren()[1]);
            nodes.push(node->getChildren()[2]);
            nodes.push(node->getChildren()[3]);
        }
    }
    return count;
}

void SplitMerge::split() {
    std::queue<Node*> nodesToSplit;
    nodesToSplit.push(root);
    
    while (!nodesToSplit.empty()) {
        Node* node = nodesToSplit.front();
        nodesToSplit.pop();
        if (node->getZone()->isUniform()) {
            zones.emplace_back(*node->getZone());
            continue;
        }
        // showImageSplit();
        int x = node->getZone()->getX();
        int y = node->getZone()->getY();
        int width = node->getZone()->getWidth();
        int height = node->getZone()->getHeight();
        int width1 = width / 2;
        int width2 = width - width1;
        int height1 = height / 2;
        int height2 = height - height1;
        Node* child1 = new Node(node, new Zone(x, y, width1, height1, img, compareColor));
        Node* child2 = new Node(node, new Zone(x + width1, y, width2, height1, img, compareColor));
        Node* child3 = new Node(node, new Zone(x, y + height1, width1, height2, img, compareColor));
        Node* child4 = new Node(node, new Zone(x + width1, y + height1, width2, height2, img, compareColor));
        node->addChild(child1);
        node->addChild(child2);
        node->addChild(child3);
        node->addChild(child4);
        adjacency.addAdjacency(child1, child3, Adjacency::RIGHT);
        adjacency.addAdjacency(child1, child2, Adjacency::BOTTOM);
        adjacency.addAdjacency(child3, child4, Adjacency::BOTTOM);
        adjacency.addAdjacency(child2, child4, Adjacency::RIGHT);
        adjacency.replaceAdjacency(node, {child1, child3}, Adjacency::TOP);
        adjacency.replaceAdjacency(node, {child1, child2}, Adjacency::LEFT);
        adjacency.replaceAdjacency(node, {child3, child4}, Adjacency::RIGHT);
        adjacency.replaceAdjacency(node, {child2, child4}, Adjacency::BOTTOM);
        nodesToSplit.push(child1);
        nodesToSplit.push(child2);
        nodesToSplit.push(child3);
        nodesToSplit.push(child4);
    }
    splitDone = true;
    std::cout << "Split in " << zones.size() << " zones" << std::endl;
}

void SplitMerge::splitMultiThread() {
    ThreadPool<std::vector<Node*>> worker (8);
    worker.addTask(std::bind(&SplitMerge::splitTask, this, root));
    while (worker.hasTasks() || worker.hasResults()) {
        // showImageSplit();
        std::vector<Node*> result = worker.getTaskResult();
        if (result.empty()) {
            continue;
        }
        worker.addTask(std::bind(&SplitMerge::splitTask, this, result[0]));
        worker.addTask(std::bind(&SplitMerge::splitTask, this, result[1]));
        worker.addTask(std::bind(&SplitMerge::splitTask, this, result[2]));
        worker.addTask(std::bind(&SplitMerge::splitTask, this, result[3]));
    }
    splitDone = true;
}

std::vector<Node*> SplitMerge::splitTask(Node* node) {
    std::vector<Node*> nodes;
    if (node->getZone()->isUniform()) {
        std::lock_guard<std::mutex> lock(mutex);
        zones.emplace_back(*node->getZone());
        return nodes;
    }
    int x = node->getZone()->getX();
    int y = node->getZone()->getY();
    int width = node->getZone()->getWidth();
    int height = node->getZone()->getHeight();
    int width1 = width / 2;
    int width2 = width - width1;
    int height1 = height / 2;
    int height2 = height - height1;
    Node* child1 = new Node(node, new Zone(x, y, width1, height1, img, compareColor));
    Node* child2 = new Node(node, new Zone(x + width1, y, width2, height1, img, compareColor));
    Node* child3 = new Node(node, new Zone(x, y + height1, width1, height2, img, compareColor));
    Node* child4 = new Node(node, new Zone(x + width1, y + height1, width2, height2, img, compareColor));
    node->addChild(child1);
    node->addChild(child2);
    node->addChild(child3);
    node->addChild(child4);
    
    std::unique_lock<std::mutex> lock(mutex);
    adjacency.addAdjacency(child1, child3, Adjacency::RIGHT);
    adjacency.addAdjacency(child1, child2, Adjacency::BOTTOM);
    adjacency.addAdjacency(child3, child4, Adjacency::BOTTOM);
    adjacency.addAdjacency(child2, child4, Adjacency::RIGHT);
    adjacency.replaceAdjacency(node, {child1, child3}, Adjacency::TOP);
    adjacency.replaceAdjacency(node, {child1, child2}, Adjacency::LEFT);
    adjacency.replaceAdjacency(node, {child3, child4}, Adjacency::RIGHT);
    adjacency.replaceAdjacency(node, {child2, child4}, Adjacency::BOTTOM);
    lock.unlock();

    nodes.push_back(child1);
    nodes.push_back(child2);
    nodes.push_back(child3);
    nodes.push_back(child4);
    return nodes;
}

void SplitMerge::merge() {
    // Copy adjacency list
    Adjacency temp = adjacency;
    // Get a random node in the graph
    Node* node = temp.getRandomNode();
    // Keep track of the explored nodes
    std::set<Node*> explored;

    int countZones = 0;
    std::cout << countZones << " zones merged, " << zones.size() << " left" << std::endl;

    while(node != nullptr) {
        std::vector<Node*> nodesToMerge;
        std::queue<Node*> nodesToExplore;
        nodesToExplore.push(node);
        // Explore the graph to find all the nodes to merge
        while (!nodesToExplore.empty()) {
            node = nodesToExplore.front();
            nodesToExplore.pop();
            if (!node->isLeaf() || explored.find(node) != explored.end()) {
                temp.removeAdjacency(node);
                continue;
            }
            explored.insert(node);
            nodesToMerge.push_back(node);
            // Get all the neighbors of the node
            std::vector<Node*> neighbors = temp.getAdjacencies(node);
            for (Node* neighbor : neighbors) {
                if (neighbor->isLeaf() && explored.find(neighbor) == explored.end()) {
                    if (compareColor(neighbor->getZone()->getColor(), node->getZone()->getColor())) {
                        // Add the neighbor to the queue to explore its neighbors
                        nodesToExplore.push(neighbor);
                    }
                }
                // Remove the adjacency between the node and its neighbor
                temp.removeAdjacency(node, neighbor);
            }
            // Remove all the adjacencies of the node
            temp.removeAdjacency(node);
        }
        // Merge the nodes
        nodesMerged.push_back(nodesToMerge);
        // Get a new random node and repeat
        node = temp.getRandomNode();

        countZones += nodesToMerge.size();
        std::cout << countZones << " zones merged, " << static_cast<int>(zones.size()) - countZones  << " left" << std::endl;
        // showImageMerge();
    }
    mergeDone = true;
}

void SplitMerge::mergeMultiThread() {
    int nbThreads = 8;
    ThreadPool<std::vector<Node*>> worker (nbThreads);
    Adjacency temp = adjacency;
    bool some = true;

    while (some) {
        std::set<Node*> nodesVisited;

        some = false;
        for(int i = 0; i < nbThreads; i++) {
            Node* node = temp.getRandomNode();
            if (node == nullptr) {
                continue;
            }
            worker.addTask(std::bind(&SplitMerge::mergeTask, this, node, std::set<Node*>()));
            some = true;
        }
        
        for (int i = 0; i < nbThreads; i++) {
            std::vector<Node*> result = worker.getTaskResult();
            if (result.empty()) {
                continue;
            }
            bool found = false;
            for (Node* node : result) {
                if (nodesVisited.find(node) != nodesVisited.end()) {
                    found = true;
                    break;
                }
                nodesVisited.insert(node);
            }
            if (found) {
                continue;
            }
            nodesMerged.push_back(result);
            for (Node* node : result) {
                temp.removeAdjacency(node);
            }
        }
    }

    mergeDone = true;
}

std::vector<Node*> SplitMerge::mergeTask(Node* node, std::set<Node*>& nodesVisited) {
    if (node == nullptr) {
        return std::vector<Node*>();
    }
    std::vector<Node*> nodesToMerge;
    if (!node->isLeaf()) {
        return nodesToMerge;
    }
    nodesToMerge.push_back(node);
    nodesVisited.insert(node);
    std::vector<Node*> neighbors = adjacency.getAdjacencies(node);
    for (Node* neighbor : neighbors) {
        if (nodesVisited.find(neighbor) == nodesVisited.end() && neighbor->isLeaf()) {
            nodesVisited.insert(neighbor);
            if (compareColor(neighbor->getZone()->getColor(), node->getZone()->getColor())) {
                std::vector<Node*> nodes = mergeTask(neighbor, nodesVisited);
                nodesToMerge.insert(nodesToMerge.end(), nodes.begin(), nodes.end());
            }
        }
    }
    return nodesToMerge;
}

std::set<Node*> SplitMerge::getNodesToMerge(Node* node, std::set<Node*>& nodesMerged, std::set<Node*>& nodesVisited) {
    nodesMerged.insert(node);
    nodesVisited.insert(node);
    std::vector<Node*> neighbors = adjacency.getAdjacencies(node);
    for (Node* neighbor : neighbors) {
        if (nodesVisited.find(neighbor) == nodesVisited.end() && neighbor->isLeaf()) {
            nodesVisited.insert(neighbor);
            if (compareColor(neighbor->getZone()->getColor(), node->getZone()->getColor())) {
                getNodesToMerge(neighbor, nodesMerged, nodesVisited);
            }
        }
    }
    return nodesMerged;
}

void SplitMerge::showImageSplit() {
    cv::Mat imgToShow = getRandomColorZones();
    // cv::Mat imgToShow = getColorZones();
    while(true) {
        cv::imshow("Split", imgToShow);
        int touche = cv::waitKey(33);
        if (touche == 27) {
            break;
        }
    }
}

void SplitMerge::showImageMerge() {
    cv::Mat imgToShow = getRandomColor();
    while(true) {
        cv::imshow("Merge", imgToShow);
        int touche = cv::waitKey(33);
        if (touche == 27) {
            break;
        }
    }
}

void SplitMerge::showNeighbors() {
    std::stack<Node*> nodesList;
    std::set<Node*> nodesExplored;
    Node* cur = root;
    while (!cur->isLeaf()) {
        cur = cur->getChildren()[0];
    }
    nodesList.push(cur);
    while(!nodesList.empty()) {
        cv::Mat imgToShow = img.clone();

        cur = nodesList.top();
        nodesList.pop();

        if (nodesExplored.find(cur) != nodesExplored.end()) {
            continue;
        }

        int x = cur->getZone()->getX();
        int y = cur->getZone()->getY();
        int width = cur->getZone()->getWidth();
        int height = cur->getZone()->getHeight();
        for (int i = x; i < x + width; i++) {
            for (int j = y; j < y + height; j++) {
                imgToShow.at<cv::Vec3b>(i, j)[2] = 255;
                imgToShow.at<cv::Vec3b>(i, j)[1] = 0;
                imgToShow.at<cv::Vec3b>(i, j)[0] = 0;
            }
        }

        for (Node* node : adjacency.getAdjacencies(cur, Adjacency::RIGHT)) {
            int x = node->getZone()->getX();
            int y = node->getZone()->getY();
            int width = node->getZone()->getWidth();
            int height = node->getZone()->getHeight();
            for (int i = x; i < x + width; i++) {
                for (int j = y; j < y + height; j++) {
                    imgToShow.at<cv::Vec3b>(i, j)[2] = 0;
                    imgToShow.at<cv::Vec3b>(i, j)[1] = 0;
                    imgToShow.at<cv::Vec3b>(i, j)[0] = 0;
                }
            }
            nodesList.push(node);
        }

        while(true) {
            cv::imshow("Neighbours", imgToShow);
            int touche = cv::waitKey(33);
            if (touche == 27) {
                break;
            }
        }

        nodesExplored.insert(cur);
    }
}

void SplitMerge::showWithColors() {
    cv::Mat imgToShow = getColors();
    while(true) {
        cv::imshow("With colors", imgToShow);
        int touche = cv::waitKey(33);
        if (touche == 27) {
            break;
        }
    }
}

void SplitMerge::setCompareColor(std::function<bool(Pixel, Pixel)> compareColor) {
    this->compareColor = compareColor;
}

cv::Mat SplitMerge::getRandomColorLeaf() {
    cv::Mat imgToShow = this->img.clone();
    getRandomColorLeafRecur(root, imgToShow);
    return imgToShow;
}

void SplitMerge::getRandomColorLeafRecur(Node* node, cv::Mat& imgToShow) {
    if (node->isLeaf()) {
        int x = node->getZone()->getX();
        int y = node->getZone()->getY();
        int width = node->getZone()->getWidth();
        int height = node->getZone()->getHeight();
        int r = rand() % 256;
        int g = rand() % 256;
        int b = rand() % 256;
        for (int i = x; i < x + width; i++) {
            for (int j = y; j < y + height; j++) {
                imgToShow.at<cv::Vec3b>(i, j)[2] = r;
                imgToShow.at<cv::Vec3b>(i, j)[1] = g;
                imgToShow.at<cv::Vec3b>(i, j)[0] = b;
            }
        }
    }
    else {
        for (Node* child : node->getChildren()) {
            getRandomColorLeafRecur(child, imgToShow);
        }
    }
}


cv::Mat SplitMerge::getRandomColorZones() {
    if (!splitDone) {
        return getRandomColorLeaf();
    }
    cv::Mat img = this->img.clone();
    for (Zone zone : zones) {
        int x = zone.getX();
        int y = zone.getY();
        int width = zone.getWidth();
        int height = zone.getHeight();
        int r = rand() % 256;
        int g = rand() % 256;
        int b = rand() % 256;
        for (int i = x; i < x + width; i++) {
            for (int j = y; j < y + height; j++) {
                img.at<cv::Vec3b>(i, j)[2] = r;
                img.at<cv::Vec3b>(i, j)[1] = g;
                img.at<cv::Vec3b>(i, j)[0] = b;
            }
        }
    }
    return img;
}

cv::Mat SplitMerge::getColorZones() {
    cv::Mat img = this->img.clone();
    for (Zone zone : zones) {
        int x = zone.getX();
        int y = zone.getY();
        int width = zone.getWidth();
        int height = zone.getHeight();
        int r = zone.getColor().getR();
        int g = zone.getColor().getG();
        int b = zone.getColor().getB();
        for (int i = x; i < x + width; i++) {
            for (int j = y; j < y + height; j++) {
                img.at<cv::Vec3b>(i, j)[2] = r;
                img.at<cv::Vec3b>(i, j)[1] = g;
                img.at<cv::Vec3b>(i, j)[0] = b;
            }
        }
    }
    return img;
}

cv::Mat SplitMerge::getRandomColor() {
    cv::Mat img = this->img.clone();
    for (std::vector<Node*> nodes : nodesMerged) {
        int r = rand() % 256;
        int g = rand() % 256;
        int b = rand() % 256;
        for (Node* node : nodes) {
            int x = node->getZone()->getX();
            int y = node->getZone()->getY();
            int width = node->getZone()->getWidth();
            int height = node->getZone()->getHeight();
            for (int i = x; i < x + width; i++) {
                for (int j = y; j < y + height; j++) {
                    img.at<cv::Vec3b>(i, j)[2] = r;
                    img.at<cv::Vec3b>(i, j)[1] = g;
                    img.at<cv::Vec3b>(i, j)[0] = b;
                }
            }
        }
    }
    return img;
}

cv::Mat SplitMerge::getColors() {
    cv::Mat img = this->img.clone();
    for (std::vector<Node*> nodes : nodesMerged) {
        for (Node* node : nodes) {
            int x = node->getZone()->getX();
            int y = node->getZone()->getY();
            int width = node->getZone()->getWidth();
            int height = node->getZone()->getHeight();
            Pixel color = node->getZone()->getColor();
            for (int i = x; i < x + width; i++) {
                for (int j = y; j < y + height; j++) {
                    img.at<cv::Vec3b>(i, j)[2] = color.getR();
                    img.at<cv::Vec3b>(i, j)[1] = color.getG();
                    img.at<cv::Vec3b>(i, j)[0] = color.getB();
                }
            }
        }
    }
    return img;
}