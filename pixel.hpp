#ifndef PIXEL_HPP
#define PIXEL_HPP

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>

class Pixel {
public:
    Pixel();
    Pixel(int r, int g, int b);
    Pixel(const cv::Vec3b& color);
    int getR() const;
    int getG() const;
    int getB() const;
    void setR(int r);
    void setG(int g);
    void setB(int b);

    bool operator==(const Pixel& pixel) const;
private:
    int r;
    int g;
    int b;
};

#endif