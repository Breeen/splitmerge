#include "adjacency.hpp"

Adjacency::Adjacency() {}

Adjacency::Adjacency(const Adjacency& adjacency) {
    // std::lock_guard<std::mutex> lock(mutex);
    adjacencys = adjacency.adjacencys;
}

Adjacency::~Adjacency() {}

bool Adjacency::isAdjacent(Node* node1, Node* node2) {
    // std::lock_guard<std::mutex> lock(mutex);
    for (int i = 0; i < 4; i++) {
        if (adjacencys[node1].find(std::make_pair(i, node2)) != adjacencys[node1].end()) {
            return true;
        }
    }
    return false;
}

void Adjacency::addAdjacency(Node* node1, Node* node2, int direction) {
    // std::lock_guard<std::mutex> lock(mutex);
    adjacencys[node1].insert(std::make_pair(direction, node2));
    adjacencys[node2].insert(std::make_pair((direction + 2) % 4, node1));
}

void Adjacency::removeAdjacency(Node* node1, Node* node2) {
    // std::lock_guard<std::mutex> lock(mutex);
    for (int i = 0; i < 4; i++) {
        adjacencys[node1].erase(std::make_pair(i, node2));
        adjacencys[node2].erase(std::make_pair(i, node1));
    }
    if (adjacencys[node1].size() == 0) {
        adjacencys.erase(node1);
    }
}

void Adjacency::removeAdjacency(Node* node, int direction) {
    // std::lock_guard<std::mutex> lock(mutex, std::adopt_lock);
    std::vector<std::pair<int, Node*>> to_remove;
    for (std::pair adjacent_pair : adjacencys[node]) {
        if (adjacent_pair.first == direction) {
            to_remove.push_back(adjacent_pair);
        }
    }
    for (std::pair adjacent_pair : to_remove) {
        Node* adjacent = (Node*)adjacent_pair.second;
        adjacencys[node].erase(std::make_pair(direction, adjacent));
        adjacencys[adjacent].erase(std::make_pair((direction + 2) % 4, node));
        if (adjacencys[adjacent].size() == 0) {
            adjacencys.erase(adjacent);
        }
    }
    if (adjacencys[node].size() == 0) {
        adjacencys.erase(node);
    }
}

void Adjacency::removeAdjacency(Node* node) {
    // std::lock_guard<std::mutex> lock(mutex);
    for (int i = 0; i < 4; i++) {
        removeAdjacency(node, i);
    }
}

void Adjacency::replaceAdjacency(Node* node, std::vector<Node*> childs, int direction) {
    // std::lock_guard<std::mutex> lock(mutex);
    for (std::pair adjacent_pair : adjacencys[node]) {
        if (adjacent_pair.first == direction) {
            Node* adjacent = (Node*)adjacent_pair.second;
            for (Node* child : childs) {
                adjacencys[adjacent].insert(std::make_pair((direction + 2) % 4, child));
                adjacencys[child].insert(std::make_pair(direction, adjacent));
            }
        }
    }
    removeAdjacency(node, direction);
}

void Adjacency::insertAdjacency(Node* node, std::vector<Node*> childs, int direction) {
    // std::lock_guard<std::mutex> lock(mutex);
    for (Node* child : childs) {
        adjacencys[node].insert(std::make_pair(direction, child));
        adjacencys[child].insert(std::make_pair((direction + 2) % 4, node));
    }
}

std::set<Node*> Adjacency::getAdjacencies(Node* node, int direction) {
    // std::lock_guard<std::mutex> lock(mutex);
    std::set<Node*> adjacencies;
    for (std::pair<int, Node*> adjacent : adjacencys[node]) {
        if (adjacent.first == direction) {
            adjacencies.insert(adjacent.second);
        }
    }
    return adjacencies;
}

std::vector<Node*> Adjacency::getAdjacencies(Node* node) {
    // std::lock_guard<std::mutex> lock(mutex);
    std::vector<Node*> adjacencies;
    for (std::pair<int, Node*> adjacent : adjacencys[node]) {
        adjacencies.push_back(adjacent.second);
    }
    return adjacencies;
}

Node* Adjacency::getRandomNode() {
    // std::lock_guard<std::mutex> lock(mutex);
    if (adjacencys.size() == 0) {
        return nullptr;
    }
    int random = rand() % adjacencys.size();
    for (std::pair<Node*, std::set<std::pair<int, Node*>>> adjacency : adjacencys) {
        if (random == 0) {
            return adjacency.first;
        }
        random--;
    }
    return nullptr;
}

size_t Adjacency::size() {
    // std::lock_guard<std::mutex> lock(mutex);
    int size = 0;
    for (std::pair<Node*, std::set<std::pair<int, Node*>>> adjacency : adjacencys) {
        size += adjacency.second.size();
    }
    return size;
}

Adjacency& Adjacency::operator=(const Adjacency& other) {
    // std::lock_guard<std::mutex> lock(mutex);
    adjacencys = other.adjacencys;
    return *this;
}