#ifndef SPLITMERGE_HPP
#define SPLITMERGE_HPP

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>

#include <functional>
#include <set>
#include <vector>
#include <mutex>

#include "node.hpp"
#include "adjacency.hpp"
#include "threadpool.hpp"

class SplitMerge {
public: 
    SplitMerge(cv::Mat& img);
    ~SplitMerge();

    Node* getRoot();
    int countLeaves();

    void split();
    void splitMultiThread();
    void merge();
    void mergeMultiThread();
    void showImageSplit();
    void showImageMerge();
    void showNeighbors();
    void showWithColors();

    void setCompareColor(std::function<bool(Pixel, Pixel)> compareColor);

    cv::Mat getRandomColorLeaf();
    cv::Mat getRandomColorZones();
    cv::Mat getColorZones();
    cv::Mat getRandomColor();
    cv::Mat getColors();

private:
    std::set<Node*> getNodesToMerge(Node* node, std::set<Node*>& nodesToMerge, std::set<Node*>& nodesVisited);

    void getRandomColorLeafRecur(Node*, cv::Mat&);

    std::vector<Node*> splitTask(Node* node);
    std::vector<Node*> mergeTask(Node* node, std::set<Node*>& nodesToMerge);

    cv::Mat& img;
    Node* root;
    Adjacency adjacency;
    std::vector<std::vector<Node*>> nodesMerged;
    std::vector<Zone> zones;
    std::function<bool(Pixel, Pixel)> compareColor;
    bool splitDone;
    bool mergeDone;
    std::mutex mutex;
};

#endif