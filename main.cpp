#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>

#include <iostream>

#include "splitmerge.hpp"

int main () {
    srand(time(NULL));

    cv::Mat img = cv::imread("../img/test1.jpg");
    // cv::Mat img = cv::imread("../img/carre.png");

    SplitMerge sm(img);
    sm.setCompareColor([](Pixel p1, Pixel p2) {
        return (p1.getR() - p2.getR()) * (p1.getR() - p2.getR()) + (p1.getG() - p2.getG()) * (p1.getG() - p2.getG()) + (p1.getB() - p2.getB()) * (p1.getB() - p2.getB()) < 50;
    });
    sm.split();
    sm.showImageSplit();
    // sm.showNeighbors();
    sm.merge();
    sm.showImageMerge();
    // sm.showWithColors();

    // sm.splitMultiThread();
    // sm.showImageSplit();
    // sm.mergeMultiThread();
    // sm.showImageMerge();

    return 0;
}