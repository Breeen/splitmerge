#ifndef ADJACENCY_HPP
#define ADJACENCY_HPP

#include <map>
#include <set>
#include <vector>
#include <mutex>

#include "node.hpp"

class Adjacency {
public:
    enum {
        TOP,
        RIGHT,
        BOTTOM,
        LEFT
    };

    Adjacency();
    Adjacency(const Adjacency& adjacency);
    ~Adjacency();

    bool isAdjacent(Node* node1, Node* node2);
    void addAdjacency(Node* node1, Node* node2, int direction);
    void removeAdjacency(Node* node1, Node* node2);
    void removeAdjacency(Node* node, int direction);
    void removeAdjacency(Node* node);
    void replaceAdjacency(Node* node, std::vector<Node*> childs, int direction);
    void insertAdjacency(Node* node, std::vector<Node*> childs, int direction);

    std::set<Node*> getAdjacencies(Node* node, int direction);
    std::vector<Node*> getAdjacencies(Node* node);
    Node* getRandomNode();

    size_t size();

    Adjacency& operator=(const Adjacency& adjacency);

private:
    std::map<Node*, std::set<std::pair<int, Node*>>> adjacencys;
    // std::mutex mutex;
};

#endif