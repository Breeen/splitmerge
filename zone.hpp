#ifndef ZONE_HPP
#define ZONE_HPP

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>

#include "pixel.hpp"

class Zone {
public:
    Zone(int x, int y, int width, int height, cv::Mat& img, std::function<bool(Pixel, Pixel)> compareColor);
    Zone(const Zone& zone);
    ~Zone();

    int getX() const;
    int getY() const;
    int getWidth() const;
    int getHeight() const;
    Pixel getColor() const;

    bool isUniform();

private:
    enum {
        UNIFORM,
        NOT_UNIFORM,
        NOT_YET_CALCULATED
    };
    int x;
    int y;
    int width;
    int height;
    int uniform;
    Pixel color;
    cv::Mat& img;
    std::function<bool(Pixel, Pixel)> compareColor;
};

#endif